module.exports = {
    env: {
        browser: true,
        es2021: true,
        'jest/globals': true,
    },
    extends: [
        'plugin:react/recommended',
        'airbnb',
    ],
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
        ecmaVersion: 12,
        sourceType: 'module',
    },
    plugins: [
        'react',
        'jest',
    ],
    rules: {
        indent: ['error', 4],
        'import/prefer-default-export': 'off',
        'react/jsx-indent': ['error', 4],
        'react/jsx-indent-props': ['error', 4],
        'react/jsx-closing-bracket-location': [2, 'tag-aligned'],
        'no-unused-vars': ['error', { vars: 'all', args: 'after-used', ignoreRestSiblings: false }],
        'react/prop-types': 'off',
    },
    overrides: [
        {
            files: ['src/index.js', 'src/App.js', 'src/App.test.js'],
            rules: {
                'react/jsx-filename-extension': 'off',
            },
        },
    ],
};
