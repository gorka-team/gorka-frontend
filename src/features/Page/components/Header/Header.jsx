import { Menu } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom';

export const Header = ({ pageKey }) => (
    <div>
        <div className="logo" />
        <Menu mode="horizontal" selectedKeys={[pageKey]}>
            <Menu.Item key="calendar">
                <Link to="/calendar">Calendar</Link>
            </Menu.Item>
            <Menu.Item key="news">
                <Link to="/">News</Link>
            </Menu.Item>
        </Menu>
    </div>
);
