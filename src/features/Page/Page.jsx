import React from 'react';
import { Header } from './components/Header/Header';

import { Board } from '../Board/Board';
import { TrainingCalendar } from '../TrainingCalendar/TrainingCalendar';

import './Page.css';

export const Page = ({ pageKey }) => (
    <div className="Page">
        <Header pageKey={pageKey} />
        <main className="Page-Content">
            {pageKey === 'news' && <Board />}
            {pageKey === 'calendar' && <TrainingCalendar />}
        </main>
    </div>
);
