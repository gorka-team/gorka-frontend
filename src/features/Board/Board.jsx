import React, { useEffect, useState } from 'react';
import { Card, Typography, Divider } from 'antd';

import './Board.css';
import './ck.css';
import { getNews } from '../../requests';

const { Title } = Typography;
const { Meta } = Card;

export const Board = () => {
    const [news, setNews] = useState([]);

    useEffect(() => getNews()
        .then((newNews) => {
            setNews((newNews || []));
        }), []);

    return (
        (
            <div className="Board">
                {
                    news.map(({ title, body, timestamp }) => (
                        <Card className="Board-Post">
                            <Title level={3}>{title}</Title>
                            <Meta description={`${new Date(timestamp).toDateString()} ${new Date(timestamp).toLocaleTimeString()}`} />
                            <Divider />
                            {/* eslint-disable-next-line react/no-danger */}
                            <div className="Board-PostContent ck-content" dangerouslySetInnerHTML={{ __html: body }} />
                        </Card>
                    ))
                }
            </div>
        )
    );
};
