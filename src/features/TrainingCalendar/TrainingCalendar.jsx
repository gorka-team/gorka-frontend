import React, {
    useCallback, useEffect, useMemo, useState,
} from 'react';
import Calendar from 'react-awesome-calendar';

import { EventPopup } from './components/EventPopup/EventPopup';

import { convertRawEvent } from './utils';
import { getTrainings } from '../../requests';

import './TrainingCalendar.css';

export const TrainingCalendar = () => {
    const [events, setEvents] = useState([]);

    useEffect(() => getTrainings()
        .then((newEvents) => {
            setEvents((newEvents || []).map(convertRawEvent));
        }), []);

    const [currentEventId, setCurrentEventId] = useState(undefined);
    const showPopup = useCallback((id) => setCurrentEventId(id), [setCurrentEventId]);
    const closePopup = useCallback(() => setCurrentEventId(undefined), [setCurrentEventId]);

    const currentEventData = useMemo(
        () => currentEventId && events.find(({ id }) => id === currentEventId),
        [events, currentEventId],
    );

    const onClickEvent = useCallback((eventId) => {
        showPopup(eventId);
    }, [showPopup]);

    return (
        <div className="TrainingCalendar">
            <Calendar
                events={events}
                onClickEvent={onClickEvent}
            />
            <EventPopup
                visible={Boolean(currentEventId)}
                eventData={currentEventData}
                closePopup={closePopup}
            />
        </div>
    );
};
