import { Modal, Divider } from 'antd';
import React, { useMemo } from 'react';

import './EventPopup.css';

export const EventPopup = ({ visible, closePopup, eventData }) => {
    const sportsman = useMemo(() => (
        eventData && eventData.sportsmen.map(
            (sportsmen) => <div className="EventPopup-Sportsmen">{sportsmen}</div>,
        )
    ), [eventData]);

    return (
        <>
            <Modal
                visible={visible}
                title={eventData && eventData.title}
                onCancel={closePopup}
                footer={null}
            >
                <div className="EventPopup-Item EventPopup-Sportsman">
                    <b>Sportsmen: </b>
                    {sportsman}
                </div>
                <Divider />
                <div className="EventPopup-Item EventPopup-Notes">
                    <div><b>Notes: </b></div>
                    {eventData && eventData.notes}
                </div>
            </Modal>
        </>
    );
};
