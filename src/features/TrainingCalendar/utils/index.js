export const countEndTime = (startTime) => new Date(
    new Date(startTime).getTime() + 60 * 60 * 1000,
).toISOString();

export const convertRawEvent = (rawEvent) => ({
    ...rawEvent,
    title: `Workout ${rawEvent.id}`,
    from: rawEvent.startTime,
    to: countEndTime(rawEvent.startTime),
    color: '#3694DF',
});
