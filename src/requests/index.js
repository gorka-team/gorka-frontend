import axios from 'axios';

axios.defaults.baseURL = 'https://iu-s21-sqr-gorka-backend.herokuapp.com';

export const getTrainings = () => axios.get('/training').then((res) => res.data && res.data.content);
export const getNews = () => axios.get('/news').then((res) => res.data && res.data.content);
