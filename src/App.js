import React from 'react';
import { Route, Switch } from 'react-router-dom';

import { Page } from './features/Page/Page';

import './App.css';

function App() {
    return (
        <div className="App">
            <Switch>
                <Route exact path="/" component={() => (<Page pageKey="news" />)} />
                <Route
                    exact
                    path="/calendar"
                    component={() => (<Page pageKey="calendar" />)}
                />
            </Switch>
        </div>
    );
}

export default App;
