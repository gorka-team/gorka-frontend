/* global cy */

const calendarStub = {
    totalElements: 1,
    content: [{
        id: 1,
        sportsmen: ['kamtim', 'imajou'],
        startTime: '2021-03-08T21:00:00Z',
        notes: 'Random workout for all students',
    }],
};

/**
 * Открыть страницу c календарем
 */
const openCalendarPage = () => {
    cy.server();
    cy.route('GET', '**/training*', calendarStub).as('training');

    cy.visit('http://localhost:3000/calendar');

    cy.wait(1000);
    cy.wait('@training');

    cy.get('.Page').toMatchImageSnapshot();

    cy.get('.eventTitle').click();
    cy.wait(1000);
    cy.get('.dailyEventWrapper').click();

    cy.wait(1000);
    cy.get('.Page').toMatchImageSnapshot();
};

describe('calendar page', () => {
    it('Should render page', () => {
        openCalendarPage();
    });
});
