/* global cy */

const newsStub = {
    totalElements: 2,
    content: [{
        id: 52,
        title: 'There is a cat',
        body: '<div style="text-align: center"><a href="https://pikabu.ru/story/kotik_7930119" target="_blank"><img data-src="https://cs12.pikabu.ru/post_img/2020/12/31/7/1609413234120575381.webp" src="https://cs12.pikabu.ru/post_img/2020/12/31/7/1609413234120575381.webp" data-large-image="https://cs12.pikabu.ru/post_img/big/2020/12/31/7/1609413234120575381.jpg" width="50%"></a></div>',
        timestamp: '2021-03-08T20:22:40.379846Z',
    }, {
        id: 53,
        title: 'Lorem ipsum dolor',
        body: '<div> <div> <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean dignissim, est eu blandit interdum, justo nisi molestie nisl, eget dapibus nisi dui eu felis. Vivamus fermentum auctor orci, nec vestibulum lectus feugiat malesuada. Fusce ut tincidunt justo. Sed ultrices condimentum magna. Donec metus est, dictum quis suscipit et, commodo id ante. Donec interdum fringilla magna convallis vulputate. Cras in pretium orci, feugiat ullamcorper ipsum. Pellentesque vel odio et tortor pharetra consectetur nec ac sem. Nunc molestie cursus augue, nec consectetur mauris feugiat et. Vivamus scelerisque ut mi eget rhoncus. Sed non purus in dolor laoreet consectetur id vel nibh. Donec vitae tortor nec diam condimentum ultricies maximus sit amet felis. Duis non lacus dignissim, egestas massa sit amet, maximus metus. Quisque elementum magna quis interdum lacinia. Praesent vulputate arcu eget lacus hendrerit placerat. Nullam non nibh sollicitudin, pharetra ipsum vel, pulvinar nisi. </p> </div> <div> <figure> <div><a href="https://cs13.pikabu.ru/post_img/big/2021/03/05/5/1614924603117721596.jpg" target="_blank"><img data-src="https://cs13.pikabu.ru/post_img/2021/03/05/5/1614924603117721596.webp" src="https://cs13.pikabu.ru/post_img/2021/03/05/5/1614924603117721596.webp" data-large-image="https://cs13.pikabu.ru/post_img/big/2021/03/05/5/1614924603117721596.jpg" data-viewable="true" width="50%"></a></div> </figure></div></div>',
        timestamp: '2021-03-08T20:33:18.255451Z',
    }],
};

/**
 * Открыть страницу c новостями
 */
const openNewsPage = () => {
    cy.server();
    cy.route('GET', '**/news*', newsStub).as('news');

    cy.visit('http://localhost:3000');

    cy.wait(1000);
    cy.wait('@news');

    cy.get('.Page').toMatchImageSnapshot();
};

describe('news page', () => {
    it('Should render page', () => {
        openNewsPage();
    });
});
